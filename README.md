> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Leah Ramsier

### Assignment 4 Requirements:

### Deliverables:
1. Provide Bitbucket read-only access to a4 repo, *must* include README.md, using Markdown
syntax.
2. Blackboard Links: a4 Bitbucket repo

### Assignment Links:

[A4](http://localhost:9999/a4/index.jsp "Assigment 4 Form and Data Validation")

### Assigment Screenshots:

![Valid Data](img/a4valid.png "Valid Data")
![Invalid Data](img/a4invalid.png "Invalid Data")
![Thanks Screen](img/a4thanks.png "Thanks Screen")




